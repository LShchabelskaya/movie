"use strict";
const url = `http://api.weatherstack.com/current`;
const citiesArr = ['Illichivsk', 'Odesa', 'Kiev', 'Auckland', 'Amsterdam'];
const ulForCities = document.querySelector('.city-list');
const cardWrapper = document.querySelector('#weather');
const inputWeather = document.querySelector('#toggle1');

window.addEventListener('load', renderInitialWindow);

function renderInitialWindow() {
    citiesArr.forEach((city) => createDropdown(city));
    requestWeatherForCity(citiesArr[0]);
};

function createDropdown(city) {
    const li = document.createElement('li');
    li.classList.add('animate');
    li.textContent = city;
    ulForCities.append(li);

    li.addEventListener('click', () => {
        const selectedLi = document.querySelector('.selected');

        if(selectedLi) {
            selectedLi.classList.remove('selected');
        };

        li.classList.add('selected');
        inputWeather.checked = false;
        inputWeather.nextElementSibling.innerText = city;
        requestWeatherForCity(city);
    });
};

async function requestWeatherForCity(city) {
    const currentCard = document.querySelector('.weather__loaded');

    if(currentCard) {
        currentCard.remove();
    };

    createLoaderCard();
    const weatherFetch = await fetch(`${url}?access_key=${localStorage.getItem('accessKey')}&query=${city}`);
    const weatherData = await weatherFetch.json();
    const finalData = weatherData.current;
    document.querySelector('.weather__loading').remove();
    createWeatherCard(city, finalData.weather_descriptions[0], finalData.weather_icons[0], finalData.temperature, finalData.feelslike);
};

function createLoaderCard() {
    const divForLoader = document.createElement('div');
    divForLoader.classList.add('weather__loading');
    const loaderImg = document.createElement('img');
    loaderImg.setAttribute('src', 'img/loading.gif');
    loaderImg.setAttribute('alt', 'Loading...');
    cardWrapper.append(divForLoader);
    divForLoader.append(loaderImg);
};

function createWeatherCard(city, wStatus, wIcon, temperature, wFeelsLike) {
    const weatherWrapper = document.createElement('div');
    weatherWrapper.classList.add('weather__loaded');
    const weatherHeader = document.createElement('div');
    weatherHeader.classList.add('weather__header');
    const weatherTemp = document.createElement('div');
    weatherTemp.classList.add('weather__temp');
    weatherTemp.textContent = temperature;
    const tempFeelsLike = document.createElement('div');
    tempFeelsLike.classList.add('weather__feels-like');
    tempFeelsLike.textContent = `Feels like: ${wFeelsLike}`;
    const weatherMain = document.createElement('div');
    weatherMain.classList.add('weather__main');
    const weatherIcon = document.createElement('div');
    weatherIcon.classList.add('weather__icon');
    const weatherCity = document.createElement('div');
    weatherCity.classList.add('weather__city');
    weatherCity.textContent = city;
    const weatherStatus = document.createElement('div');
    weatherStatus.classList.add('weather__status');
    weatherStatus.textContent = wStatus;
    const iconImg = document.createElement('img');
    iconImg.setAttribute('src', wIcon);
    iconImg.setAttribute('alt', 'image');
    weatherWrapper.append(weatherHeader, weatherTemp, tempFeelsLike);
    cardWrapper.append(weatherWrapper);
    weatherHeader.append(weatherMain, weatherIcon);
    weatherMain.append(weatherCity, weatherStatus);
    weatherIcon.append(iconImg);
};