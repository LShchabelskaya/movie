import { Person } from './Person';

export class Veterinarian extends Person {
    #diagnosis = {
        ill: 'ill', 
        healthy: 'healthy',
    };
    
    constructor(firstName, lastName, hospital) {
        super(firstName, lastName);
        this.hospital = hospital;
    }

    getFullName() {
        return `${super.getFullName()}(${this.hospital.name})`;
    }

    getFindOrLeaveResult(animal) {
        const findHomeResult = this.hospital.findHome(animal);

        if (findHomeResult.status === 'success') {
            return {
                diagnosis: this.#diagnosis.healthy,
                info: `change home. Now ${findHomeResult.name} has a new friend - ${animal.nickname}`
            }
        } else {
            return {
                diagnosis: this.#diagnosis.ill,
                info: findHomeResult.message,   
            }
        }
    }

    #setDiagnosis(animal) {
        if(animal.weight > 20) {
            return {
                diagnosis: this.#diagnosis.ill,
                info: 'overweight',
            };
        } else if(animal.food === 'dry food') {
            animal.changeFood('meal with rice');
            return {
                diagnosis: this.#diagnosis.ill,
                info: `change food. Now ${animal.nickname} eats ${animal.food}`,
            };
        } else if (animal.isHomless) {
            return this.getFindOrLeaveResult(animal);
        }
        return {
            diagnosis: this.#diagnosis.healthy,
            info: `${animal.nickname} is healthy!`,
        };
    }

    treatAnimal(animal) {
        const setDiagnosisResult = this.#setDiagnosis(animal);
        
        if(setDiagnosisResult.diagnosis === 'ill') {
            this.hospital.addAnimal(animal);
            animal.diagnosis = setDiagnosisResult.info;
            return {
                info: `${animal.nickname} from ${animal.location}`,
                fullDiagnos: `${setDiagnosisResult.diagnosis}: ${setDiagnosisResult.info}`,
            };
        };
        return {
            info: `${animal.nickname} from ${animal.location}`,
            fullDiagnos: `${setDiagnosisResult.diagnosis}: ${setDiagnosisResult.info}`,
        };
    }
}