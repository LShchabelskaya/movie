import { Animal } from './Animal';

export class Cat extends Animal {
    constructor(nickname, food, location) {
        super(nickname, food, location);
        this.isHomless = true;
    }
}