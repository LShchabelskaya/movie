//Задача 1
//Реализуем небольшую визуализацию больничного крыла.

//Создать класс Animal со свойствами nickname, food, location и методом changeFood().

//Создать классы Dog и Cat, которые наследуются от класса Animal и имеют дополнительные свойства 
//(weight для класса Dog, isHomless (по-умолчанию true) для класса Cat).

//Создать класс Person со свойствами firstName и lastName и методом getFullName().

//Создать класс Hospital со свойством name, а также с приватными свойствами #illAnimals и #findingPetsPeople 
//и методами:
// - для получения данных из массивов: getAnimals() и getFindingPetsPeople();
// - для добавления данных в массивы: addAnimal() и addPeople() (можно добавить сразу много людей);
// - findHome() - проверяет находится ли животное в больнице, если да, возвращает объект:
//______________//
// return {
//     status: restricted,
//     message: We need to heal <nickname> firstly,
// };
//______________//
//если, нет - удаляет случайного человека из массива #findingPetsPeople и возвращает объект:
//______________//
// return {
//   status: success,
//   name: <firstName> <lastName>,
// };
//______________//

//Создать класс Veterinarian, который наследуется от класса Person и имеет новое свойство hospital и 
//приватное (1) свойство #diagnosis = { ill: 'ill', healthy: 'healthy' }, а также следующие методы:
// - переопределенный метод getFullName(), который возвращает имя врача и название больницы, 
//где он работает (например, Ivan Ivanov (Hospital));
// - #setDiagnosis() (приватный метод), который возвращает объект диагноза пациенту
//______________//
// return {
//    diagnosis: healthy or ill diagnosis from #diagnosis property,
//    info: some message to user,
// };
//______________//

// - Если у животного вес больше 20кг, то вернуть ill и 'overweight';
// - Если животное ест корм, то изменить питание на 'meal with rice' и вернуть ill и 'change food.Now <nickname> eats <food>;
// - Если животное бездомное, то найти животному нового хозяина и если все хорошо, то вернуть healthy 
//и 'change home. Now <firstName> <lastName> have a new friend - <nickname>', если нет - вернуть ill и сообщение, 
//которое возвращает метод;
// - иначе вернуть healthy;

// 1. treatAnimal(), вызывает метод #setDiagnosis() - (если животное нужно лечить, добавляет его в больницу) и возвращает объект:
//______________//
// return {
//   info: <nickname> from <location>,
//   fullDiagnos: <diagnosis>: <info>,
// }
//______________//

//Создайте функцию main(), в которой создайте больницу, врача, несколько разных типов животных и несколько людей, которые ищут себе животных.
//Всех животных отправьте на прием к врачу и выведете в консоль информацию о процессе лечения.
//______________//
// console.group(veterinar.getFullName());
// console.log(conclusion.info);
// console.log(conclusion.fullDiagnos);
// console.groupEnd();
//______________//
//Выведите в консоль клички всех животных, которые лечатся в больнице.
//______________
// console.log('Animals in the hospital: ' + illAnimalNicknames);
//______________//

//P.S. По желанию вы можете расширить логику лечения в методе #setDiagnosis и добавить новые диагнозы, 
//а также добавлять новые свойства в классы по своему желанию. Следите только, чтобы все они были используемы

import './styles/style.css';
import { Dog } from './Dog';
import { Cat } from './Cat';
import { Hospital } from './Hospital';
import { Person } from './Person';
import { Veterinarian } from './Veterinarian';
import { Vetassistant } from './Vetassistant';

function main() {
    const hospital = new Hospital('VETMedical');

    const dog1 = new Dog('Fluffy', 'meat', 'Chornomorsk', 14);
    const dog2 = new Dog ('Richard', 'dry food', 'Kyiv', 19);
    const dog3 = new Dog ('Boy', 'bones', 'Lutsk', 21);
    const cat1 = new Cat('Sid', 'dry food', 'Vinnytsya');
    const cat2 = new Cat('Tom', 'meat', 'Poltava');
    const cat3 = new Cat('Liya', 'wet food', 'Lviv');
    const animals = [dog1, dog2, dog3, cat1, cat2, cat3];

    const person1 = new Person('Hanna', 'Leksyuk');
    const person2 = new Person('Vitaliy', 'Kim');
    const person3 = new Person('Igor', 'Petrenko');
    const person4 = new Person('Anton', 'Sobko');
    hospital.addPeople(person1, person2, person3, person4);

    const veterinarian = new Veterinarian('Oleksiy', 'Grinchenko', hospital);
    console.group(veterinarian.getFullName());
    animals.forEach((animal) => {
        const conclusion = veterinarian.treatAnimal(animal);
        console.log(`${conclusion.info}\n${conclusion.fullDiagnos}`);
    });
    console.groupEnd();

    const vetassistant = new Vetassistant('Nadiya', 'Stepanova', hospital);
    const illAnimals = hospital.getAnimals();
    const illAnimalNicknames = [];
    console.group(vetassistant.getFullName());
    illAnimals.forEach((animal) => {
        illAnimalNicknames.push(animal.nickname);
        const prescription = vetassistant.setPrescription(animal);
        console.log(`${prescription.medicine}\n${prescription.animalPickUpDate}`)
    });
    console.groupEnd();

    console.log('\nAnimals in the hospital: ' + illAnimalNicknames);
};

main();