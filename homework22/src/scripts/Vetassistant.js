import { getRandomIntInclusive } from './helper func';
import { Veterinarian } from './Veterinarian';

export class Vetassistant extends Veterinarian {
    constructor(firstName, lastName, hospital) {
        super(firstName, lastName, hospital);
    }

    getFullName() {
        return `${super.getFullName()}, assistant`;
    }

    setPrescription(animal) {
        const farmacy = this.hospital.getFarmacy();
        const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

        if(animal.diagnosis === 'overweight') {
            return {
                medicine: farmacy.overweight,
                animalPickUpDate: `Please come for ${animal.nickname} next ${weekDays[getRandomIntInclusive(0, weekDays.length - 1)]}`,
            };
        } else if(animal.isHomless) {
            return {
                medicine: farmacy.isHomless,
                animalPickUpDate: `Please come for ${animal.nickname} next ${weekDays[getRandomIntInclusive(0, weekDays.length - 1)]}`,
            };
        }
        return {
            medicine: farmacy.changeFood,
            animalPickUpDate: `Please come for ${animal.nickname} next ${weekDays[getRandomIntInclusive(0, weekDays.length - 1)]}`,
        };
    }
}