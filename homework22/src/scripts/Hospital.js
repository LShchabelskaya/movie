import { getRandomIntInclusive } from './helper func';

export class Hospital {
    #illAnimals = [];
    #findingPetsPeople = [];
    #farmacy = {
        overweight: 'heart protectors (pills)',
        isHomless: 'flea and tick treatment',
        changeFood: 'vitamins',
    };

    constructor(name) {
        this.name = name;
    }

    getAnimals() {
        return this.#illAnimals;
    }

    getFindingPetsPeople() {
        return this.#findingPetsPeople; 
    }

    getFarmacy() {
        return this.#farmacy;
    }

    addAnimal(animal) {
        this.#illAnimals.push(animal);
    }

    addPeople() {
        this.#findingPetsPeople.push(...arguments);
    }

    findHome(animal) { 
        const foundAnimal = this.#illAnimals.find((illAnimal) => illAnimal.nickname === animal.nickname);

        if(foundAnimal) {
            return {
                status: 'restricted',
                message: `We need to heal ${animal.nickname} firstly`,
            }; 
        };
        
        const animalHost = this.#findingPetsPeople.splice(getRandomIntInclusive(0, this.#findingPetsPeople.length - 1), 1);
        animal.isHomless = false;
        return {
            status: 'success',
            name: animalHost[0].getFullName(),
        };
    }
}