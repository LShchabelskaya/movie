//Задача 1
//Напишіть програму, яка питає у користувача його імʼя і виводить в консолі текстове привітання.
//Happy birthday to you
//Happy birthday to you
//Happy birthday, dear <name>
//Happy birthday to you

const userName = prompt('Please enter your name', 'Lena');

for(i = 1; i <= 4; i++) {
    if(i !== 3) {
        console.log(`Happy birthday to you`);
    } else {
        console.log(`Happy birthday, dear ${userName}`);
    }
}

//Задача 2
//Сформуйте строку '.#.#.#.#.#.#.#' за допомогою циклу for, де необхідну кількість повторів символів '.#' 
//задає користувач через команду prompt().

const repeatsQuantity = +prompt('Please enter a number of repeats:', '2');
let initialString = '';
const symbolToRepeat = '.#';

if(isNaN(repeatsQuantity) || !Number.isInteger(repeatsQuantity) || repeatsQuantity <= 0) {
    console.error('Your value provided is not a correct number!')
} else {
    for(i = 1; i <= repeatsQuantity; i++) {
        initialString += symbolToRepeat;
    }
    console.log(initialString);
}

//Задача 3
//Напишіть програму, яка питає у користувача число і сумує всі непарні числа до цього числа.
//Якщо користувач ввів не число або відʼємне число чи 0, визивати команду prompt() з текстом "Invalid. You should enter a number" до тих пір поки вірний формат даних не буде введений користувачем.
//Результат відобразіть в консолі.

let userNumber = +prompt('Please enter a number:', '5');
let oddSum = 0;

if(isNaN(userNumber) || userNumber <= 0 || !Number.isInteger(userNumber)) {
    do {
        userNumber = +prompt('Invalid. You should enter a number');
    } while (isNaN(userNumber) || userNumber <= 0 || !Number.isInteger(userNumber));
}

for(let i = 1; i <= userNumber; i += 2) {
    oddSum += i;
}
console.log(`Sum of odd numbers is ${oddSum}`);



//Задача 4
//Напишіть нескінченний цикл, який закінчується командою break, коли Math.random() > 0.7. 
//Виведіть в консоль число, на якому переривається цикл та відобразіть в консолі кількість ітерацій циклу.
//Loop was finished because of: <number>
//Number of attempts: <number>

let randomNumber = Math.random().toFixed(1);

for(let i = 1; true; i++) {
    if(randomNumber <= 0.7) {
        randomNumber = Math.random().toFixed(1);
    } else {
        console.log(`Loop was finished because of: ${randomNumber}\nNumber of attempts: ${i}`);
        break;
    }
}

//Задача 5
//Напишіть цикл від 1 до 50, в якому будуть виводитися числа по черзі від 1 до 50, при цьому:
//Якщо число ділиться на 3 без залишку, то виведіть не це число, а слово 'Fizz';
//Якщо число ділиться на 5 без залишку, то виведіть не це число, а слово 'Buzz';
//Якщо число ділиться і на 3 і на 5 одночасно, то виведіть не це число, а слово 'FizzBuzz'

const numbersQuantity = 50;
const dividedByThree = 'Fizz';
const dividedByFive = 'Buzz';

for(let i = 1; i <= numbersQuantity; i++) {
    if(i % 3 === 0 && i % 5 === 0) {
        console.log(dividedByThree + dividedByFive);
    } else if(i % 3 === 0) {
        console.log(dividedByThree);
    } else if(i % 5 === 0 ) {
        console.log(dividedByFive);
    } else {
        console.log(i);
    }
}

//Задача 6

//Напишіть програму, яка знайде всі роки, коли 1 січня випадає на неділю у період між 2015 та 2050 роками включно 
//(зверніть увагу, що 1 січня в лапках).
//"1st of January" is being a Sunday in [year]

const startYear = 2015;
const endYear = 2050;

for(let i = startYear; i <= endYear; i++) {
    const dateNeeded = new Date(i, 0, 1);
    if(dateNeeded.getDay() === 0) {
        console.log(`"1st of January" is being a Sunday in ${i}`);
    }
}