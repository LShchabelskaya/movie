//Задача
//Создайте класс Student, который принимает в качестве аргумента в конструкторе объект enrollee (абитурент). 
//У экземпляра класса Student должны быть поля:

// -id - уникальный идентификатор студента (генерируется при создании экземпляра и начинается с 1);
// -name - имя студента (передаем в объекте enrollee);
// -surname - фамилия студента (передаем в объекте enrollee);
// -ratingPoint - рейтинг студента по результатам вступительных экзаменов (передаем в объекте enrollee);
// -schoolPoint - рейтинг студента по результатам ЗНО (передаем в объекте enrollee);
// -isSelfPayment - если true, то студент на контракте, если false - на бюджете (генерируется по логике указанной ниже).

//tips: для генерации id, можно создать отдельное статическое свойство в классе (static id = 1) 
//и обращаться потом к нему в любом месте класса (Student.id)

//Создайте класс University со свойством name и методом addStudent().
//Метод addStudend() должен реализовывать следующую логику:
//Добавлять студентов в массив студентов университета;
//Если у студента ratingPoint больше или равен 800, то студент может быть зачислен на бюджет, 
//при условии что он будет входить в пятерку (5) лучших студентов рейтинга (напомню, что рейтинг считается как сумма 
//результатов ЗНО и вступительных экзаменов - schoolPoint и ratingPoint);
//Устанавливать правильное значение isSelfPayment для каждого студента;
//Добавьте так же методы для получения данных о списках зачисленных студентов и студентов, которые зачислены на бюджет.
//____________________
//Проверьте работу программы на примере массива students, который прикреплен в качестве файла ниже. Создайте объект студента на основе всех 
//абитуриентов из массива students и добавьте их в университет. Выведите информацию о студентах, 
//которые поступили в университет и тез, кто зачислен на бюджет.
//При желании можете добавить метод getStudentsByRating() для более читабельного вывода данных.

class Student {
    static id = 1;

    constructor(enrollee) {
        this.id = Student.id++;
        this.name = enrollee.name;
        this.surname = enrollee.surname;
        this.ratingPoint = enrollee.ratingPoint;
        this.schoolPoint = enrollee.schoolPoint;
        this.isSelfPayment = true;
        this.rating = this.ratingPoint + this.schoolPoint;
    }
}

class University {
    constructor(name, students) {
        this.name = name;
        this.students = students;
    }

    addStudent(student) {
        this.students.push(student);
        this.students
            .filter((student) => student.ratingPoint >= 800)
            .sort((student1, student2) => student1.rating > student2.rating ? -1 : 1)
            .slice(0, 5)
            .forEach((student) => student.isSelfPayment = false);
    }

    getStudentsList() {
        return this.students;
    }

    getFreeOfPaymentStudentsList() {
        return this.students.filter((student) => student.isSelfPayment === false);
    }

    getStudentsByRating(list) {
        return list.sort((student1, student2) => student1.rating > student2.rating ? -1 : 1);
    }
}

const studentsOdessaNationalUniversity = students.map((student) => new Student(student));
const university = new University('ONU', studentsOdessaNationalUniversity);
const newStudent = new Student({ name: 'Ann', surname: 'Bill', ratingPoint: 2000, schoolPoint: 630 });
university.addStudent(newStudent);
const studentsList = university.getStudentsList();
const freeOfPaymentStudentsList = university.getFreeOfPaymentStudentsList();
console.log('All students sorted:', university.getStudentsByRating(studentsList));
console.log('Free of payment students sorted:', university.getStudentsByRating(freeOfPaymentStudentsList));














// var items = [
//     { name: 'Edward', value: 21 },
//     { name: 'Sharpe', value: 37 },
//     { name: 'And', value: 45 },
//     { name: 'The', value: -12 },
//     { name: 'Magnetic' },
//     { name: 'Zeros', value: 37 }
// ];
// items.sort(function (a, b) {
//     if (a.value > b.value) {
//     return 1;
//     }
//     if (a.value <= b.value) {
//     return -1;
//     }
// });

// items.sort(function(a, b) {
//     return a.value - b.value;
// });
// console.log(items);