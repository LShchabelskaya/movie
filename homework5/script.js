/*
  1) Напишіть програму, яка рахує, в якій чверті на годиннику знаходиться вказане користувачем число в хвилинах. 
Перевірте, щоб число було цілим і в проміжку між 0 і 59. Результат вивести в консоль.

Якщо number === 15, то виведе 2
Якщо number === 48, то виведе 4
Якщо number === 59, то виведе 4
Якщо number === 150, то виведе повідомлення про помилку
*/

const userMinutesNumberStr = prompt('Please enter the number of minutes 0 - 59:', '15');
const userMinutesNumber = +userMinutesNumberStr;
const maxMinute = 59;
const quarter = 15;

if(isNaN(userMinutesNumber) || 
  userMinutesNumber < 0 || 
  userMinutesNumber > maxMinute || 
  !Number.isInteger(userMinutesNumber) || 
  isNaN(parseFloat(userMinutesNumberStr))) {  /* тут у цій та 2й задачі придумала, як додати помилку на '' та ' '. В прикладах раніше вони не проходили, бо ми виключали 0 також.*/
  console.error('Please provide the number within 0 - 59 only!');
} else {
    if(userMinutesNumber >= 0 && userMinutesNumber < quarter) {
      console.log('This is a 1st quarter!');
    } else if(userMinutesNumber >= quarter && userMinutesNumber < 2 * quarter) {
      console.log('This is a 2nd quarter!');
    } else if(userMinutesNumber >= 2 * quarter && userMinutesNumber < 3 * quarter) {
      console.log('This is a 3rd quarter!');
    } else {
      console.log('This is a 4th quarter!');
    }
  }

/* 
  2) Напишіть програму, яка питає у користувача число
  і виводить в консоль наступну інформацію про нього:
  - додатнє воно чи від'ємне;
  - скільки цифр в цьому числі;
  - якщо число додатнє, виведіть суму його цифр

    Якщо number === 0, то виведе '0, lenght: 1'
    Якщо number === 100500, то виведе 'positive, length: 6, sum: 6'
    Если number === -50, то виведе 'negative, length: 2'
*/

const userNumAsStr = prompt('Please enter an integer number:', '5');
const userNumber = +userNumAsStr;
const userNumAsStrLength = userNumAsStr.length;
let userNumSum = 0;

if(isNaN(userNumber) || !Number.isInteger(userNumber) || isNaN(parseFloat(userNumAsStr))) {
  console.error('Plese use only integer numbers!');
} else {
  if(userNumber === 0) {
    console.log(`${userNumber}, length: ${userNumAsStrLength}`);
  } else if(userNumber > 0) {
    for(let i = 0; i < userNumAsStrLength; i++) {
      const userNumSymbol = +userNumAsStr.charAt(i);
      userNumSum += userNumSymbol;
    }
    console.log(`positive, length: ${userNumAsStrLength}, sum: ${userNumSum}`);
  } else {
    console.log(`negative, length: ${userNumAsStrLength - 1}`);
  }
}

/*
  3) Відомо, что подорож на Мальдиви коштує 3000$, а купити нові AirPods - 300$.
  Напишіть програму, яка питає у користувача число (в $) та
  виводить в консоль інформацію, що він за ці гроші може купити.

    Якщо money === 200$, то виведе 'You can't do anything. I'm sorry :(';
    Якщо money === 300$, то виведе 'You can buy only AirPods';
    Якщо money === 3200.50$, то виведе 'You can go on vacation or buy AirPods! What are you waiting for?';
    Якщо money === 4300.53, то виведе 'You have enough money for everything. WOW!'
*/

const journeyPrice = 3000;
const airPodsPrice = 300;
const allPurchasesPrice = journeyPrice + airPodsPrice;
const userMoneySumStr = prompt('Please enter your sum of money in $:', '500$');
const userMoneySumNum = parseFloat(userMoneySumStr);

if(isNaN(userMoneySumNum) || userMoneySumNum < 0) {
  console.error('Please use 0 or positive numbers only!');
} else {
  if(userMoneySumNum < airPodsPrice) {
    console.log('You can\'t do anything. I\'m sorry :(');
  } else if(userMoneySumNum >= airPodsPrice && userMoneySumNum < journeyPrice) {
    console.log('You can buy only AirPods');
  } else if(userMoneySumNum >= journeyPrice && userMoneySumNum < allPurchasesPrice) {
    console.log('You can go on vacation or buy AirPods! What are you waiting for?');
  } else {
    console.log('You have enough money for everything. WOW!');
  }
}

/*
  4) Напишіть програму, яка питає у користувача число,
  виводить на экран всі числа від 1 до цього числа 
  та зводить у ступінь 2 кожне ПАРНЕ його число

    Якщо number === 5, то виведе '1 4 3 16 5'
*/

const userIntNumber = +prompt('Please enter an integer positive number:', '5');
let rowOfNumbers = '';

if(isNaN(userIntNumber) || userIntNumber <= 0 || !Number.isInteger(userIntNumber)) {
  console.error('Please use positive integer numbers greater than 0!');
} else {
  for(let i = 1; i <= userIntNumber; i++) {
    const iExponent = i ** 2;
    if(i % 2 === 0) {
      rowOfNumbers += `${iExponent} `;
    } else {
      rowOfNumbers += `${i} `;
    }
  }
  console.log(rowOfNumbers);
}

/*
  6) Напишіть програму, яка питає у користувача символ та число
  і виводить цей символ послідовно, збільшуючи кожен раз на 1, поки 
  кількість символів в рядку не буде дорівнювати цьому числу.

    Якщо sybmol === #, number === 5, то виведе
    #
    ##
    ###
    ####
    #####

    Якщо sybmol === @, number === 6, то виведе
    @
    @@
    @@@
    @@@@
    @@@@@
    @@@@@@
*/

const userSymbol = prompt('Please enter any symbol to repeat:', '*');
const userNumOfRepeats = +prompt('Please enter the number of symbol repeats:', '5');
let rowOfSymbols = '';

if(isNaN(userNumOfRepeats) || userNumOfRepeats <= 0 || !Number.isInteger(userNumOfRepeats)) {
  console.error('Please use positive integer numbers greater than 0!');
} else {
  for(let i = 1; i <= userNumOfRepeats; i++) {
    rowOfSymbols += userSymbol;
    console.log(rowOfSymbols);
  }
}

/*
  7) Напишіть цикл, який заповнює строку value числами від 1000 до 2000
  і додайте до кожного числа символи '&#'. Результат первірте в браузері,
  запустив index.html файл.

    Формат відповіді:
    console.log(value); // &#1000 &#1001 &#1002 ... &#1999 &#2000

  upd: Перевірте також код для значень від 7000 до 10000
*/

let result = document.getElementById('result');
let value = '';
const sybmolToAdd = '&#';
const maxNumber = 10000;
const minNumber = 7000;

for(let i = minNumber; i <= maxNumber; i++) {
  value += `${sybmolToAdd}${i} `;
}

result.innerHTML = value;
