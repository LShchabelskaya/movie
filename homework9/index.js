//Задача 1
//Напишіть функцію fillArray, яка створює масив і заповнює його заданим значенням.
//let array = fillArray(3, 'qwerty');
//console.log(array); // ['qwerty', 'qwerty', 'qwerty']
//де 3 - це довжина масиву, а 'qwerty' значення кожного елементу масива

function fillArray(length, item) {
    return new Array(length).fill(item);
};

let array1 = fillArray(3, 'qwerty');
console.log(array1);

//Задача 2
//Напишіть функцію reverseArray, яка перевертає значення масиву задом наперед.
//let array = ['My', 'life', '-', 'my', 'rules'];
//let result = reverseArray(array);
//сonsole.log(result); // ['rules', 'my', '-', 'life', 'My'];

function reverseArray(arr) {
    return [...arr].reverse();
};

let array2 = ['My', 'life', '-', 'my', 'rules'];
let result2 = reverseArray(array2);
console.log(result2);

//Задача 3
//Напишіть функцію filterArray, яка очищує масив від небажаних значень (false, undefined, '', 0, null, NaN).
//let array = [0, 1, 2, null, undefined, 'qwerty', false, NaN];
//let result = filterArray(array);
//console.log(result); // [1,2, 'qwerty'];

function filterArray(arr) {
    const arrFiltered = arr.filter((item) => item);
    return arrFiltered;
};

let array3 = [0, 1, 2, null, undefined, 'qwerty', false, NaN];
let result3 = filterArray(array3);
console.log(result3);

//Задача 4
//Напишіть функцію spliceFour, яка видаляє 4й елемент масиву і замінює його строкою 'JavaScript'
//let array = [1, 2, 3, 4, 5];
//let result = spliceFour(array);
//console.log(result); // [1, 2, 3, 'JavaScript', 5];

function spliceFour(arr) {
    const arrSpliced = arr.splice(3, 1, 'JavaScript');
    return arr;
};

let array4 = [1, 2, 3, 4, 5];
let array4Copy = [...array4];
let result4 = spliceFour(array4Copy);
console.log(result4);

//Задача 5
//Напишіть функцію joinArray, яка перетворює масив в строку, з'єднуючи елементи заданим символом.
//let array = [1, 2, 3, 4, 5];
//let result = joinArray(array, '%');
//console.log(result); // 1%2%3%4%5

function joinArray(arr, symbol) {
    return arr.join(symbol);
};

let array5 = [1, 2, 3, 4, 5];
let result5 = joinArray(array5, '%');
console.log(result5);

//Задача 6
//Напишіть функцію joinStr, яка повертає строку, яка утворена з конкатинації усіх строк, 
//які передані в якості аргументів функції через кому.
// let string1 = joinStr(0);
// console.log(string1); // ''
// let string2 = joinStr(1,'hello',3, 'world');
// console.log(string2); // 'hello,world'
// let string3 = joinStr('g','o', 0, '0', null, 'd', {});
// console.log(string3); // 'g,o,0,d'

function joinStr() {
    const argsFilteredToStr = Array
    .from(arguments)
    .filter((item) => typeof item === 'string')
    .join();
    return argsFilteredToStr;
};

let string1 = joinStr(0);
console.log(string1);
let string2 = joinStr(1,'hello',3, 'world');
console.log(string2);
let string3 = joinStr('g','o', 0, '0', null, 'd', {});
console.log(string3);

//Задача 7
//Напишіть функцію advancedFillArray, яка створює масив і заповнює його випадковими значеннями чисел з заданого діапазону. 
//(Логіку створення випадкового числа оберніть в функцію setRandomValue(min, max)).
//let array  = advancedFillArray(10, 1, 15),
//де 10 - це довжина масива, 1 - це мінімальне значення, 15 - це максимальне значення.
//tips: тут вам станеться у нагоді функція fillArray, яку ви вже написали в першому завданні, 
//та метод масиву map для того, щоб змінити значення на потрібні.

function advancedFillArray(length, min, max) {
    return fillArray(length, 0).map(() => setRandomValue(min, max)); 
};

function setRandomValue(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

let array7  = advancedFillArray(10, 1, 15);
console.log(array7);