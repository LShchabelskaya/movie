//Задача 1

//Создайте функцию createBuffer(), которая создает локальную переменную text в виде пустой строки и возвращает функцию buffer, 
//с которой можно работать следующим образом:
//Если в функцию buffer был передан строковый параметр – этот параметр записывается в переменную text. Иначе выведите ошибку в консоль.
//Если функция buffer вызывается без параметров – возвращается значение переменной text.
//Создайте переменную и запишите в нее результат выполнения функции createBuffer().
//Продемонстрируйте работу возвращаемой функции buffer с параметром и без.

function createBuffer() {
    let text = '';
    function buffer(value) {
        if(typeof value === 'string') {
            return text += value;
        } else if(!arguments.length) {
            return text;
        } else {
            return 'ERROR: Please use string parameter only!';
        }
    };
    return buffer;
};

let buffer = createBuffer();
console.log(buffer()); // ''
buffer('A');
buffer('XYZ');
console.log(buffer()); // AXYZ
console.log(buffer(5)); //'ERROR: Please use string parameter only!'

//Задача 2
//Создайте функцию signUp(), которая принимаем параметры userName, password и возвращает объект со свойством userName и методом signIn().
//Метод signIn() принимает параметр newPassword и сравнивает его значение со значением password.
//Если пароли совпадают – метод возвращает сообщение 'Sign in success for ${userName}' , иначе – 'Password is incorrect' .
//Создайте переменную user и запишите в нее результат вызова функции signUp().
//Результат работы функции выведите в консоль.

// let user = signUp("billy", "qwerty");
// user.signIn("a"); // Password is incorrect
// user.signIn("qwerty"); // Sign in success for billy

function signUp(userName, password) {
    let user = {
        userName,
        signIn: function(newPassword) {
            newPassword === password ? console.log(`Sign in success for ${userName}`) : console.log('Password is incorrect');
        }
    };
    return user;
};

let user = signUp("billy", "qwerty");
user.signIn("a"); // Password is incorrect
user.signIn("qwerty"); // Sign in success for billy