//Задача 1
//Допрацюйте масив employee, з яким ви вже працювали раніше.
//1.1) Додайте у масив два нові обʼєкти (ключі повинні бути такі ж, а значення придумайте самі).
//1.2) Виведіть в консоль масив жінок-співробітниць з досвідом роботи менше ніж 10 місяців.
//1.3) Виведіть в консоль обʼєкт співробітника, у якого id=4.
//1.4) Виведіть в консоль масив прізвищ співробітників.

//1.1
const employeeToAdd1 = {
    id: 12,
    name: 'Irena',
    surname: 'Davidson',
    salary: 1850,
    workExperience: 14,
    isPrivileges: false,
    gender: 'female',
};

const employeeToAdd2 = {
    id: 13,
    name: 'Derek',
    surname: 'Charleston',
    salary: 950,
    workExperience: 3,
    isPrivileges: true,
    gender: 'male',
};

const employeeUpd = JSON.parse(JSON.stringify(employee));/*так как push - мутабельный, скопировала полностью массив,чтоб никакой связи с предыдущим у него не было*/
employeeUpd.push(employeeToAdd1, employeeToAdd2);
console.log('New array of employees increased:', employeeUpd);

//1.2
const femaleArr = employee.filter(item => item.gender === 'female' && item.workExperience < 10);
console.log('All women with work experience less than 10 months:', femaleArr);

//1.3
const emplId4 = employee.find(item => item.id === 4);
console.log('Employee with id 4 is:', emplId4);

//1.4
const surnamesArr = employee.map(item => item.surname);
console.log('All employee surnames:', surnamesArr);

//Задача 2
//Створіть масив frameworks зі значеннями: 'AngularJS', 'jQuery'
//a. Додайте на початок масиву значення 'Backbone.js'
//b. Додайте до кінця масиву значення 'ReactJS' і 'Vue.js'
//с. Додайте в масив значення 'CommonJS' другим елементом масиву
//d. Знайдіть та видаліть із масиву значення 'jQuery' (потрібно знайти індекс елемента масиву) і виведіть його в консоль зі словами “Це тут зайве”

const frameworks = new Array('AngularJS', 'jQuery');
console.log(frameworks);

//a
frameworks.unshift('Backbone.js');
console.log('Added to the beginning:', frameworks);

//b
frameworks.push('ReactJS', 'Vue.js');
console.log('Added to the end:', frameworks);

//c
frameworks.splice(2, 0, 'CommonJS');
console.log('2nd element was added:', frameworks);

//d
const elementDeleted = frameworks.splice(frameworks.indexOf('jQuery'), 1).toString();
console.log('Це тут зайве:', elementDeleted);

//Задача 3
//Створіть функцію removeNegativeElements, яка видаляє з вхідного масиву всі негативні числа.

let result1 = removeNegativeElements([-9, 2, 3, 0, -28, 'value']); // [2, 3, 0, 'value'];
let result2 = removeNegativeElements([-9, -21, -12]); // []
let result3 = removeNegativeElements(['-102', 102]); // ['-102', 102]
let result4 = removeNegativeElements([NaN, 45, -5, null]); // [NaN, 45, null]

function removeNegativeElements(arr) {
    return arr.filter(item => !(typeof item === 'number' && item < 0));
};

console.log(result1);
console.log(result2);
console.log(result3);
console.log(result4);


//Задача 4
//Створіть функцію getStringElements, яка повертає вхідний масив лише зі строковими значеннями.

let arr1 = [1 , true , 42 , "red" , 64 , "green" , "web" , new Date() , -898 , false]
let result5 = getStringElements(arr1); // ["red", "green", "web"]

function getStringElements(arr) {
    return arr.filter(item => typeof item === 'string');
};

console.log(result5);

//Задача 5
//Напишіть функцію flatArray, яка перетворює масив масивів на єдиний масив.
//tips: вам поможет в этом метод reduce() c начальным значением [] и spread оператор.

let array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
let result6 = flatArray(array); // [1, 2, 3, 4, 5, 6, 7, 8, 9] 

function flatArray(arr) {
    return arr.reduce((accumulator, currentValue) => {
        accumulator.push(...currentValue);
        return accumulator;
    }, []);   
};

console.log(result6);

//Задача 6
//Створіть функцію myMap, яка приймає як аргумент масив та функцію, як цей масив має бути модифікований.

let arr2 = [1, 2, 3, 4];

function increaseElement (element) {
  return element * 2;
};

function myMap(arr, func) {
    return arr.map(item => func(item));
};

let result7 = myMap(arr2, increaseElement) // [2, 4, 6, 8];
console.log(result7);