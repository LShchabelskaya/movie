//Задача 1
//Напишіть програму, яка питає у користувача номер автобуса.
//Якщо це номер 7, 255 или 115, тоді користувач може їхати. Виведіть в консолі "You can go".
//Якщо ні - виведіть в консолі "Please wait".

const busNumber = +prompt('Please enter your bus number (numerical value):');

if(isNaN(busNumber)) {
    console.log('The value provided is not a number!');
} else if(busNumber === 7 || busNumber === 255 || busNumber === 115) {
    console.log('You can go');
} else {
    console.log('Please wait');
}

//Задача 2
//Напишіть програму, яка отримує від користувача число і порівнює його з числом Pi. (Math.PI)
//Порівняйте, чи є це число більше ніж число Pi.
//Порівняйте, чи є це число менше ніж число Pi.
//Порівняйте, чи це число дорівнює числу Pi.
//Якщо введене значення не є числов, виведіть в консоль повідомлення про помилку.
//Всі результати відобразіть в наступному форматі:
//You entered: <number> 
//Is greater then PI: true
//Is less then PI: false
//Is equal PI: false

const numberToCompare = +prompt('Please enter your number to compare with PI:');

if(isNaN(numberToCompare)) {
    console.log('The value provided is not a number!');
} else {
    const isNumberGreater = numberToCompare > Math.PI;
    const isNumberLess = numberToCompare < Math.PI;
    const isNumberEqual = numberToCompare === Math.PI;
    console.log(`You entered: ${numberToCompare}\nIs greater than PI: ${isNumberGreater}\nIs less than PI: ${isNumberLess}\nIs equal PI: ${isNumberEqual}`);
}

//Задача 3
//Напишіть програму, яка пропонує користувачу ввести пароль і перевіряє, чи є ций пароль надійним за наступними умовами:
//-Пароль повинен бути не менше шести символів;
//-Пароль не повинен бути рівним строкам qwerty чи 123456;
//-Пароль повинен мати хоча б одну велику літеру.
//Якщо всі умови виконані, виведіть в консоль повідомлення "Strong".
//Якщо пароль має хоча б одну велику літеру але складається з п'яти символів, виведіть в консоль повідомлення "Middle".
//У всіх інших випадках, виведіть в консоль повідомлення "Weak".

const userPassword = prompt('Please enter your password here:');

if(userPassword.length >= 6 && userPassword !== 'qwerty' && userPassword !== '123456' && userPassword.toLowerCase() !== userPassword) {
    console.log('Strong');
} else if(userPassword.length === 5 && userPassword.toLowerCase() !== userPassword) {
    console.log('Middle');
} else {
    console.log('Weak');
}

//Задача 4
//Напишіть програму, яка питає у користувача номер квартири (команда prompt()) і виводить в консоль номер поверху і номер під'їзду.
//Відомо, що в одному під'їзді 9 поверхів по 3 квартири на кожному поверсі. 
//Результат (поверх і під'їзд) відобразіть в консолі (команда console.log()).
// 3 x 9 = 27 квартир/під'їзд 

//(номери 1 - 27) - 1й під'їзд, (номери 28 - 54) - 2й під'їзд, (номери 55 - 81) - 3й під'їзд і тд

//(номери 1 - 3) - 1й поверх + (номери 28 - 30) + (номери 55 - 57)
//(номери 4 - 6) - 2й поверх + (номери 31 - 33) + (номери 58 - 60)
//(номери 7 - 9) - 3й поверх + (номери 34 - 36) + (номери 61 - 63)
//(номери 10 - 12) - 4й поверх + (номери 37 - 39) + (номери 64 - 66)
//(номери 13 - 15) - 5й поверх + (номери 40 - 42) + (номери 67 - 69)
//(номери 16 - 18) - 6й поверх + (номери  43 - 45) + (номери 70 - 72)
//(номери 19 - 21) - 7й поверх + (номери 46 - 48) + (номери 73 - 75)
//(номери 22 - 24) - 8й поверх + (номери 49 - 51) + (номери 76 - 78)
//(номери 25 - 27) - 9й поверх + (номери 52 - 54) + (номери 79 - 81)

const roomsOnFloor = 3;
const floors = 9;
const roomNumber = +prompt('Please provide your appartment number:');
const roomsPerEntrance = roomsOnFloor * floors;
const entranceNumber = Math.ceil(roomNumber / roomsPerEntrance);

if(entranceNumber === 1) {
    const floorNumber = Math.ceil(roomNumber / 3);
    console.log(`Your floor number is ${floorNumber}\nYour entrance number is ${entranceNumber}`);
} else {
    const floorNumber = Math.ceil((roomNumber - ((entranceNumber - 1) * roomsPerEntrance)) / roomsOnFloor);
    console.log(`Your floor number is ${floorNumber}\nYour entrance number is ${entranceNumber}`);
}

//Задача 5
//Напишіть програму, яка питає у користувача температуру в Цельсіях и конвертує її в Фаренгейти. Результат відобразіть в консолі.
//Формула для конвертації: Цельсій х 1,8 + 32 = Фаренгейт
//tips: градус Цельсія в юнікоді буде "\u2103", щоб відобразити його в строці. Спробуйте самостійно знайти символ для позначення градусу Фаренгейта.
//60°C is 140°F

const celsiusTemp = +prompt('Please enter a temperature in Celsius:');
const fahrenheitTemp = celsiusTemp * 1.8 + 32;
console.log(`${celsiusTemp}\u2103 is ${fahrenheitTemp.toFixed(1)}\u2109`);
