//Задача 1
//Создайте 3 переменных со следующими значениями:

let string1 = "Aquamarine Black Blue Brown Chocolate ";
let string2 = "Green Lime Olive Orange Purple ";
let string3 = "Red Tomato Violet White Yellow";

//а) Создайте функцию joinColor(), которая объединит все 3 строки в одну. 
//Функция должна работать с любым количеством входящих строк.

function joinColor() {
    return [...arguments].join('');
};

let colors = joinColor(string1, string2, string3); // Aquamarine Black Blue Brown Chocolate Green Lime Olive Orange Purple Red Tomato Violet White Yellow
console.log(colors);

//b) Створіть функцію indexColor(), яка повертає індекс першого входження вхідної строки в строці. 
//Функція приймає два параметри:
// -строку, в якої виконується пошук (наприклад, colors);
// -строку, індекс якої, потрібно знайти

function indexColor(initialStr, strToFind) {
    return initialStr.indexOf(strToFind);
};

console.log(indexColor(colors, 'Green')); // 38

//c) Створіть функцію isColorIncludes(), яка перевіряє чи є такий колір в заданій строці чи ні. 
//Зверніть увагу, що функція повинна працювати з будь-яким регістром.

function isColorIncludes(initialStr, strToCheck) {
    return initialStr.toLowerCase().includes(strToCheck.toLowerCase());
};

console.log(isColorIncludes(colors, 'Black')); // true
console.log(isColorIncludes(colors, 'BlAcK')); // true
console.log(isColorIncludes(colors, 'Lilac')); // false

//d) Створіть функцію replaceColor(), яка знаходить строку в заданій строці і замінює іі на іншу

function replaceColor(initialStr, strToChange, strToAdd) {
    return initialStr.replace(strToChange, strToAdd);
};

let result = replaceColor(string2, 'Olive', 'Grey'); // "Green Lime Grey Orange Purple "
console.log(result);

//e) Створіть функцію splitColors(colors, numbers), яка розбиває вхідну строку на окремі слова і повертає тільки 
//ті слова, кількість яких не менше ніж число, яке передане в якості другого аргументу функції.

function splitColors(initialStr, number) {
    return initialStr.split(' ').filter((item) => item.length > number).join(' ');
};

let filteredColors = splitColors(colors, 6); // "Aquamarine Chocolate"
console.log(filteredColors);

//f) Створіть функцію calculateSpaces(), яка рахує кількість пробілів в строці.

function calculateSpaces(initialStr) {
    return initialStr.split(' ').length - 1;
};

let count = calculateSpaces(string3); // 4
console.log(count);
