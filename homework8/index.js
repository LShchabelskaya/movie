//Задача 1

//а) Створіть об'єкт vegetable, з такими властивостями, щоб наступний код повернув 
//"onions are usually white".

//б) Додайте в об'єкт vegetable такі властивості, щоб в консоль вивелося 
//"onions are usually round".

const vegetable = {
    name: 'onion',
    color: 'white',
    shape: 'round',
};

const {name, color} = vegetable;
console.log(`${name}s are usually ${color}`);

const {shape} = vegetable;
console.log(`${name}s are usually ${shape}`);

//в) Cтворіть масив об'єктів vegetables (кожен елемент масиву, повинен мати таку ж саму 
//структуру як і об'єкт vegetable) так, щоб наступний код повернув 
//"cucumbers are usually green", "tomatoes are usually red"

const vegetables = [
    {
        name: 'cucumber',
        color: 'green',
        shape: 'long',
    },
    {
        name: 'tomato',
        color: 'red',
        shape: 'round',
    },
    {
        name: 'pumpkin',
        color: 'orange',
        shape: 'round',
    }
];

const [cucumber, tomato] = vegetables;
console.log(`${cucumber.name}s are usually ${cucumber.color}`);
console.log(`${tomato.name}es are usually ${tomato.color}`);

const [firstVegetable, ...otherVegetables] = vegetables;
console.log(firstVegetable.name === cucumber.name);

const [, , pumpkin] = vegetables;
console.log(`${pumpkin.name}s are usually ${pumpkin.color}`);

//г) На основі прикладів, які показані вище, створіть масив об'єктів students, 
//виведіть 2й елемент масиву і довжину масиву елементів, що залишилися.

let students = [
    { name: 'Kate', age: 25 },
    { name: 'Artur', age: 30 },
    { name: 'Nick', age: 15 },
    { name: 'Alex', age: 28 },
    { name: 'Zhenia', age: 45 },
];

const [, secondStudent, ...otherStudents] = students;
console.log(`The second student is ${secondStudent.name}, age - ${secondStudent.age}\nOther students quantity is ${otherStudents.length}`);
