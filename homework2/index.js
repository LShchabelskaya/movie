//Задача 1
//Создайте три переменные. Присвойте первой переменной числовое значение. Вторая переменная должна быть равна первой переменной, увеличенной в 3 раза. 
//Третья переменная равна сумме двух первых. Выведите в консоль все три переменные.

const numberFirst = 24;
const numberSecond = numberFirst * 3;
const numberThird = numberFirst + numberSecond;
console.log(`a = ${numberFirst}\nb = ${numberSecond}\nc = ${numberThird}`);

//Задача 2
//Создайте переменные х и y для хранения числа. Значения переменных задает пользователь через команду prompt(). Рассчитайте произведение, частное, разность и сумму этих значений. 
//Результат последовательно отобразите в консоли в формате полной математической операции:
//a + b = c;
//a - b = c;
//a * b = c;
//a / b = c;

const numberX = +prompt('Please enter х');
const numberY = +prompt('Please enter у');
const sum = numberX + numberY;
const deduct = numberX - numberY;
const multipl = numberX * numberY;
const divis = numberX / numberY;
console.log(`${numberX} + ${numberY} = ${sum}\n${numberX} - ${numberY} = ${deduct}\n${numberX} * ${numberY} = ${multipl}\n${numberX} / ${numberY} = ${divis}`);

//Задача 3
//Создайте переменную str и запишите в нее значение из prompt. Переведите ее в верхний регистр, 
//посчитайте длину строки и выведите эти данные в консоль в форматe (это все должно быть записано в одном console.log()):
//You wrote: "<str>" \ it's length <number of str characters>.
//This is your big string: "<STR>".
//  And this is a small one: "<str>"

const str = prompt('Please enter some text');
const strLength = str.length;
const strUpper = str.toUpperCase();
const strLower = str.toLowerCase();
console.log(`You wrote: "${str}" \\ it's length ${strLength}.\nThis is your big string: "${strUpper}".\n\tAnd this is a small one: "${strLower}"`);

//Задача 4
//В пачке бумаги 500 листов. За неделю в офисе расходуется 1200 листов. 
//Какое наименьшее количество пачек бумаги нужно купить в офис на 8 недель? Результат отобразите в консоли (команда console.log()).

const sheetsInReamPaper = 500;
const consumptionPerWeek = 1200;
const weeksAmount = 8;
const consumptionPerPeriod = consumptionPerWeek * weeksAmount / sheetsInReamPaper;
const consumptionPerPeriodInteger = Math.ceil(consumptionPerPeriod);
console.log(`Number of packages: ${consumptionPerPeriodInteger}`);

//Задача 5
//Напишите программу которая считает процент от суммы по кредиту. Процент и сумму получаем от пользователя (команда prompt()), результат выводим в консоль (команда console.log()). 
//Учитывайте, что пользователь может отправить процент как 10 или же как 10%.

const percent = prompt('Please enter a loan interest rate:', '12.5%');
const creditSum = +prompt('Please enter an amount of credit:');
const percentSum = +parseFloat(percent) / 100 * creditSum;
console.log(`Sum of percent is ${percentSum.toFixed(2)}`);
