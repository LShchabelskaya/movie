//Задача
//Напишіть програму, яка питає у користувача дату в форматі YYYY-MM-DD 
//і виводить в консоль всі дати від заданої дати до сьогоднішньої.

//Обовʼязкові критерії:

// - дата повинна бути валідна і в строго заданому форматі YYYY-MM-DD (краще за все перевіряється за допомогою 
//регулярних виразів);
// - формат виводу даних довільний, але обовʼязково відображати день;

//tips: Зверніть увагу на граничні дані, після останнього дня в червні, дата перейшла до липня. 
//Це автоматично відбувається, якщо працювати з датою через клас Date

const initialDate = prompt('Please enter a date in YYYY-MM-DD format:', '2022-07-25');
const isInitialDateValid = isValidDate(initialDate);

if(!isInitialDateValid) {
    console.error('Please enter a valid date in correct format!');
} else {
    const date = {
        from: new Date(initialDate),
        to: new Date(new Date().setHours(0,0,0,0)),
        [Symbol.iterator]() {
            return {
                startDate: this.from,
                todayDate: this.to,
                next() {
                    if(this.startDate < this.todayDate) {
                        return {done: false, value: this.startDate.setDate(this.startDate.getDate() + 1)};
                    };
                    return {done: true, value: this.startDate.setDate(this.startDate.getDate() + 1)};   //не дуже зрозуміла, навіщо сюди також передавати те ж значення value, що і в попереньому return, але в класі ти передавала, тому зробила з класного прикладу :)
                },
            };
        },
    };

    for (let value of date) {
        console.warn('Date: ', new Date(value).toISOString().slice(0,10));
    };
};

function isValidDate(dateString) {
    const regExCondition = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regExCondition)) return false;
    const fullDate = new Date(dateString);
    const dateNum = fullDate.getTime();
    if(!dateNum) return false;
    return fullDate.toISOString().slice(0,10) === dateString;
};