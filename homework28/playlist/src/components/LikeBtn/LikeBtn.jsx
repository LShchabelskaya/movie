import React from 'react';
import './LikeBtn.css';

function LikeBtn({ song, likeSong }) {
    return (
        <button className='like' onClick={likeSong}>{song.isLiked ? 'Unlike' : 'Like'}</button>
    );
}

export default LikeBtn;