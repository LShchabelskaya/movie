import React from 'react';
import LikeBtn from '../LikeBtn/LikeBtn';
import DeleteBtn from '../DeleteBtn/DeleteBtn';
import LikeImg from '../LikeImg/LikeImg';
import './SongItem.css';

function SongItem({ song, deleteSong, likeSong }) {
    return (
        <li className='item'>
            <LikeImg song={song} />
            <span>{song.name}</span>
            <div className='btns-wrapper'>
                <LikeBtn  song={song} likeSong={() => likeSong(song.id)} />
                <DeleteBtn deleteSong={() => deleteSong(song.id)} />
            </div>
        </li>
    );
}

export default SongItem;