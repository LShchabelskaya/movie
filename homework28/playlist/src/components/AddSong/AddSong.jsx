import { useState } from 'react';
import './AddSong.css';

function AddSong({ addNewSong, songsList }) {
    const [newSong, setNewSong] = useState('');
    const [errorMsg, setErrorMsg] = useState({
        isVisible: false,
        text: null,
    });

    const onChange = function (event) {
        setNewSong(event.target.value);
    };

    const onClick = function (event) {
        event.preventDefault();
        const song = {
            name: newSong,
            isLiked: false,
        };
        const songMatchedArr = [...songsList].filter(item => item.name === song.name);
        if(song.name.length <= 3 || !song.name) {
            setErrorMsg({
                isVisible: true,
                text: 'Song name should not be empty or shorter than 3 characters!',
            });
        } else if(songMatchedArr.length) {
            setErrorMsg({
                isVisible: true,
                text: 'Such song has been already added!',
            });
        } else {
            setErrorMsg({
                isVisible: false,
                text: null,
            })
            addNewSong(song);
            setNewSong('');
        };
    };

    return (
        <>
            <form>
                <input
                    value={newSong}
                    onChange={onChange}
                    className='input-box'
                    type='text'
                    placeholder='Song...'
                />
                <input
                    value='Add new song'
                    onClick={onClick}
                    className='button add'
                    type='button'
                />
            </form>
            {errorMsg.isVisible ? (
                <p className='error-text'>{errorMsg.text}</p>
            ) : null}
        </>
    );
}

export default AddSong;