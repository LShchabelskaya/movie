import React from 'react';
import './DeleteBtn.css';

function DeleteBtn({ deleteSong }) {
    return (
        <button className='delete' onClick={deleteSong}>Delete</button>
    );
}

export default DeleteBtn;