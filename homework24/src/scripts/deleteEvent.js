import { URL } from './constants';

export function deleteBtnClickHandler() {
    const deleteBtns = document.querySelectorAll('.delete__property');
    deleteBtns.forEach(btn => btn.addEventListener('click', async (e) => {
        const eventToDelete = e.target.closest('.event');
        const getId = eventToDelete.querySelector('[data-id]').getAttribute('data-id');
        await (await fetch(`${URL}/${getId}`, {
            method: 'DELETE',
        }).then(() => {
            eventToDelete.remove();
        }));
    }));
};