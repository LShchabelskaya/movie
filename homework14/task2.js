//Задача 2
//Напишите функцию, которая спрашивает у пользователя разрешения добавить картинку (confirm()) 
//и в случае согласия добавляет картинку на страницу (ссылку на картинку пользователь должен задавать самостоятельно).

function addImg() {
    const isImgAdd = confirm('Do you want to add an image?');
    if(isImgAdd) {
        const imgUrl = prompt('Please share an image link below:');
        const imgTag = document.createElement('img');
        document.querySelector('#wrapper').append(imgTag);
        imgTag.setAttribute('src', imgUrl);
        imgTag.style.display = 'block';
    };
};

addImg();