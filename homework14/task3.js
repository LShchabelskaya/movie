//Задача 3
//Запросите у пользователя число и добавьте в форму теги <input> (перед кнопкной Register) равных этому числу.

//Требования к тегу <input>:
//-каждый инпут должен содержать класс input-item, value = `Input ${index}`;
//-последний инпут должен иметь дополнительный класс margin-zero;
//-создайте собственный класс, со свойством background-color и добавьте его всем нечетным инпутам;
//-очистите значение каждому третьему инпуту и задайте ему атрибут placeholder с любым текстом.

const userNumber = +prompt('Please enter an integer positive number:', '5');

if(isNaN(userNumber) ||  userNumber <= 0 || !Number.isInteger(userNumber)) {
    console.error('Please use positive integer numbers greater than 0!');
} else {
    const inputsArr = new Array(userNumber).fill('').map(function(item, i) {
        const input = document.createElement('input');
        input.setAttribute('class', 'input-item');
        input.setAttribute('value', `Input ${i + 1}`);
        document.querySelector('.button').before(input);
        return input;
    });

    inputsArr[userNumber - 1].classList.toggle('margin-zero');

    const oddInputs = document.querySelectorAll('.input-item:nth-child(odd)');

    for(let item of oddInputs) {
        item.style.backgroundColor = '#cec8ee';
    };

    const thirdInputs = document.querySelectorAll('.input-item:nth-child(3n + 3)');
    
    for(let item of thirdInputs) {
        item.removeAttribute('value');
        item.setAttribute('placeholder', 'Write something here...');
    };
};






