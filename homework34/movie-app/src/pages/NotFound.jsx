import React from 'react';

function NotFound() {
    return <h2>Sorry, page is not found 🤷‍♀️</h2>
};

export default NotFound;