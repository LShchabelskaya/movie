//Задача 1
//Создайте объект coffeeMachine со свойством message: ‘Your coffee is ready!’ и методом start(), 
//при вызове которого через 3 секунды в консоль выведется сообщение, записанное в свойстве message.

let coffeeMachine = {
    message: 'Your coffee is ready!',
    start: function() {
        setTimeout(() => console.log(this.message), 3000);
    },
};

coffeeMachine.start(); // 'Your coffee is ready!'

//Создайте объект teaPlease со свойством message: 'Wanna some tea instead of coffee?'. 
//Обновите методу start() контекст так, чтобы он выводил сообщение с нового объекта.

let teaPlease = {
    message: 'Wanna some tea instead of coffee?',
};

coffeeMachine.start.bind(teaPlease)(); // 'Wanna some tea instead of coffee?'

//Задача 2
//Напишите функцию concatStr(), которая соединяет две строки, разделенные каким-то символом: разделитель и строки передаются в параметрах функции. 
//Используя привязку аргументов с помощью bind, создайте новую функцию hello(), которая которая выводит приветствие тому, кто передан в ее параметре:

function concatStr(str1, separator, str2) {
    return str1 + separator + str2;
};

const hello = concatStr.bind({}, 'Hello', ' ');

let checkConcat = concatStr('Hello', ' ', 'Matt');
console.log(checkConcat); // 'Hello Matt'
let finalResult1 = hello('Matt'); 
console.log(finalResult1); // 'Hello Matt'
let finalResult2 = hello('John'); 
console.log(finalResult2); // 'Hello John'

//Задача 3
//Напишите функцию showNumbers(), которая последовательно выводит в консоль числа в заданном диапазоне, 
//с заданным интервалом (все данные должны передаваться как параметры функции).
//tips: для реализации используйте функцию setInterval()

function showNumbers(min, max, interval) {
    const arrOfNumbers = new Array(max - min + 1).fill(min).map((item, index) => item + index);
    let i = 0;
    const intervalId = setInterval(() => {
        console.log(arrOfNumbers[i]);
        i++;
        if(i > arrOfNumbers.length - 1) {
            clearInterval(intervalId);
        };
    }, interval);
};

showNumbers(5, 10, 500); // 5 6 7 8 9 10

//Задача 4
//Какой результат выполнения будет у данного кода? Объяснить почему.

function addBase(base) {
    return function(num) {
        return base + num;
    };
};

let addOne = addBase(1);
alert(addOne(5) + addOne(3));

/* відповідь - 10. В змінну addOne ми зберегли результат роботи функції addBase з параметром 1, тобто
анонімну функцію function(num), яка  повертає наразі 1 + num.
Далі ми вже викликаємо цю ж саму функцію, але вже зі значеннями для параметру num (5 і 3), проте ця функція
памʼятає, що змінна base залишилась у замкненні зі значенням 1, тому addOne(5) повертає 5 + 1, а addOne(3)
повертає 3 + 1, разом - 10.
*/