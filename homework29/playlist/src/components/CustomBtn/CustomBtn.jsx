import React from 'react';
import './CustomBtn.css';

function CustomBtn({ className, clickHandler, text }) {
    return <button className={className} onClick={clickHandler}>{text}</button>
}

export default CustomBtn;