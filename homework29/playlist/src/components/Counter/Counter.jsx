import { useContext} from 'react';
import './Counter.css';
import { PlaylistContextComponent } from '../../context/Context';

function Counter() {
  const [{ songsList }] = useContext(PlaylistContextComponent);
  const counter = songsList.length;

  return (
    <p className='count-title'>Count of songs:
      <span>{counter}</span>
    </p>
  );
}

export default Counter;