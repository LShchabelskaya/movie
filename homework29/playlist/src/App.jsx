import PlaylistContext from './context/Context';
import PlaylistWrapper from './components/PlaylistWrapper/PlaylistWrapper';
import './App.css';

function App() {

    return (
        <PlaylistContext>
            <PlaylistWrapper />
        </PlaylistContext>
    );
}

export default App;