import { Fragment } from 'react';
import { songs } from './data';

function Counter() {
  const counter = songs.length;

  return (
    <Fragment>
      <p className='count-title'>Count of songs:
        <span className='count'>{counter}</span>
      </p>
    </Fragment>
  );
}

export default Counter;