import { Fragment, useState } from 'react';
import { songs as songsList } from './data';
import SongItem from './SongItem';

function Songs() {
    const songsState = useState(songsList);
    return (
        <Fragment>
            <div className='songs-wrapper'>
                <ul className='songs'>
                    {songsState[0].map((songInfo, index) => (
                        <SongItem song={songInfo} key={index} />
                    ))}
                </ul>
            </div>
        </Fragment>
    );
}

export default Songs;