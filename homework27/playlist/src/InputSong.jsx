import { Fragment } from 'react';

function InputSong() {
    return (
        <Fragment>
            <input className='input-box' type='text' placeholder='Song...' />
        </Fragment>
    );
}

export default InputSong;