import { Fragment } from 'react';

function AddButton() {
    return (
        <Fragment>
            <input className='button add' type='button' value='Add new song'/>
        </Fragment>
    );
}

export default AddButton;