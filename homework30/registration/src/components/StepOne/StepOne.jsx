import { useState } from 'react';
import { increaseStep } from '../../reducer/LoginFormReducer';
import { inputHandler } from '../../helpers/inputHandler';
import TextField from '@mui/material/TextField';
import Header from '../Header/Header';
import ButtonGroup from '@mui/material/ButtonGroup';
import CustomButton from '../UI/CustomButton/CustomButton';
import * as yup from 'yup';

const schemaName = yup.string().matches(/^[a-zA-Zа-яА-Я]+$/i).required();
const schemaEmail = yup.string().email().required();

function StepOne({ state, dispatch }) {

    const [isValid, setIsValid] = useState({
        firstNameIsValid: true,
        lastNameIsValid: true,
        emailIsValid: true,
    });

    const validateFirstStep = async () => {
        const firstNameIsValid = await schemaName.isValid(state.firstName);
        const lastNameIsValid = await schemaName.isValid(state.lastName);
        const emailIsValid = await schemaEmail.isValid(state.email);

        if (firstNameIsValid && lastNameIsValid && emailIsValid) {
            dispatch(increaseStep());
        }

        setIsValid({
            ...isValid,
            firstNameIsValid,
            lastNameIsValid,
            emailIsValid,
        });
    };

    return (
        <>
            <Header stepName={'Personal info'} />
            <TextField
                label='First name'
                variant='outlined'
                id='firstName'
                value={state.firstName}
                onChange={(e) => inputHandler(e, dispatch)}
                error={!isValid.firstNameIsValid}
                helperText={isValid.firstNameIsValid ? '' : 'Please use letters only. / Can\'t be empty.'}
            />
            <TextField
                label='Last name'
                variant='outlined'
                id='lastName'
                value={state.lastName}
                onChange={(e) => inputHandler(e, dispatch)}
                error={!isValid.lastNameIsValid}
                helperText={isValid.lastNameIsValid ? '' : 'Please use letters only. / Can\'t be empty.'}
            />
            <TextField
                label='E-mail'
                variant='outlined'
                id='email'
                value={state.email}
                onChange={(e) => inputHandler(e, dispatch)}
                error={!isValid.emailIsValid}
                helperText={isValid.emailIsValid ? '' : 'E-mail format should be qwerty@qwe.com. / Can\'t be empty.'}
            />
            <ButtonGroup sx={{ justifyContent: 'end' }}>
                <CustomButton
                    clickHandler={() => validateFirstStep()}
                    text={'Next'}
                />
            </ButtonGroup>
        </>
    );
};

export default StepOne;