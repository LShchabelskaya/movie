import Header from '../Header/Header';
import AvatarField from '../UI/AvatarField/AvatarField';
import { Button } from '@mui/material';
import TextField from '@mui/material/TextField';
import CustomButton from '../UI/CustomButton/CustomButton';
import ButtonGroup from '@mui/material/ButtonGroup';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from '../../colorTheme/colorTheme';
import { changeData, decreaseStep, increaseStep } from '../../reducer/LoginFormReducer';
import { createRef } from 'react';

function StepThree({ state, dispatch }) {

    const updateImage = (event) => {
        const reader = new FileReader();
        const file = event.target.files[0];

        if (file) {
            reader.readAsDataURL(file);
        };

        reader.onloadend = () => {
            dispatch(changeData({
                [event.target.id]: {
                    src: reader.result,
                    name: file.name,
                },
            }));
        };
    };

    const fileInputRef = createRef();

    function chooseFile() {
        fileInputRef.current.click();
    };

    return (
        <>
            <Header stepName={'Avatar'} />
            <AvatarField state={state} />
            <ThemeProvider theme={theme} >
                <Button 
                    variant='outlined' 
                    color='primary'
                    onClick={chooseFile}
                >
                    Choose file
                </Button>
            </ThemeProvider>
            <label>
                <TextField
                    sx={{ display: 'none' }}
                    id='avatar'
                    type='file'
                    onChange={updateImage}
                    ref={fileInputRef}
                />
            </label>
            <ButtonGroup sx={{ justifyContent: 'space-between' }}>
                <CustomButton
                    clickHandler={() => dispatch(decreaseStep())}
                    text={'Previous'}
                />
                <CustomButton
                    clickHandler={() => dispatch(increaseStep())}
                    text={'Next'}
                />
            </ButtonGroup>
        </>
    );
};

export default StepThree;