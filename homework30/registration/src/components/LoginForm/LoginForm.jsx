import Box from '@mui/material/Box';
import './LoginForm.css';
import { useReducer } from 'react';
import { initialState, LoginFormReducer } from '../../reducer/LoginFormReducer';
import StepView from '../StepView/StepView';

function LoginForm() {
    const [state] = useReducer(LoginFormReducer, initialState);

    return (
            <Box
                component='form'
                sx={{'& > :not(style)': { m: 1, width: '25ch' }}}
                autoComplete='off'
            >
                <StepView step={state.step} />
            </Box>
    );
}

export default LoginForm;