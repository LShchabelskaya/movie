import * as React from 'react';
import './Header.css';

function Header({ stepName }) {
    return (
        <h1>✨{stepName}✨</h1>
    );
}

export default Header;