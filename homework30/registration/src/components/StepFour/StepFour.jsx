import Header from '../Header/Header';
import AvatarField from '../UI/AvatarField/AvatarField';
import TextField from '@mui/material/TextField';
import ButtonGroup from '@mui/material/ButtonGroup';
import CustomButton from '../UI/CustomButton/CustomButton';
import SimpleDialog from '../UI/SimpleDialog/SimpleDialog';
import { decreaseStep } from '../../reducer/LoginFormReducer';
import { handleToggleModal } from '../../helpers/modalHandler';

function StepFour({ state, dispatch }) {
    return (
        <>
            <Header stepName={'Conclusion'} />
            <AvatarField state={state} />
            <TextField
                label='First name'
                defaultValue={state.firstName}
                InputProps={{
                    readOnly: true,
                }}
            />
            <TextField
                label='Last name'
                defaultValue={state.lastName}
                InputProps={{
                    readOnly: true,
                }}
            />
            <TextField
                label='E-mail'
                defaultValue={state.email}
                InputProps={{
                    readOnly: true,
                }}
            />
            <ButtonGroup sx={{ justifyContent: 'space-between' }}>
                <CustomButton
                    clickHandler={() => dispatch(decreaseStep())}
                    text={'Previous'}
                />
                <CustomButton
                    clickHandler={() => handleToggleModal(dispatch)}
                    text={'Finish'}
                />
            </ButtonGroup>
            <SimpleDialog 
                onClose={() => handleToggleModal(dispatch)} 
                open={state.isOpenModal} 
                text={'👌 You\'re all set!'}
            >
                <ButtonGroup sx={{ justifyContent: 'center', paddingBottom: 3 }}>
                    <CustomButton
                        clickHandler={() => handleToggleModal(dispatch)}
                        text={'Done!'}
                    />
                </ButtonGroup>
            </SimpleDialog>
        </>
    );
};

export default StepFour;