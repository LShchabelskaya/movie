import StepOne from '../StepOne/StepOne';
import StepTwo from '../StepTwo/StepTwo';
import StepThree from '../StepThree/StepThree';
import StepFour from '../StepFour/StepFour';              
import { useReducer } from 'react';                  
import { initialState, LoginFormReducer } from '../../reducer/LoginFormReducer';

function StepView() {
    const [state, dispatch] = useReducer(LoginFormReducer, initialState);

    switch (state.step) {
        case 1:
            return <StepOne state={state} dispatch={dispatch} />
        case 2:
            return <StepTwo state={state} dispatch={dispatch} />
        case 3:
            return <StepThree state={state} dispatch={dispatch} />
        case 4:
            return <StepFour state={state} dispatch={dispatch} />
        default:
            return null;
    };
};

export default StepView;