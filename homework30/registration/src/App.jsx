import LoginForm from './components/LoginForm/LoginForm';
import LoginFormContext from './context/LoginFormContext';

function App() {
  return (
    <LoginFormContext>
      <LoginForm />
    </LoginFormContext>
  );
}

export default App;