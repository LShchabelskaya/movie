import { createContext, useReducer } from 'react';
import { LoginFormReducer, initialState } from '../reducer/LoginFormReducer';

export const LoginFormContextComponent = createContext({});

function LoginFormContext({ children }) {
    const [ state, dispatch] = useReducer(LoginFormReducer, initialState);
    return (
        <LoginFormContextComponent.Provider value={[state, dispatch]}>
            {children}
        </LoginFormContextComponent.Provider>
    )
}

export default LoginFormContext;