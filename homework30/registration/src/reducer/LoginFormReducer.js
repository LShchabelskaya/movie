export const initialState = {
    step: 1,
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    avatar: {
        src: '',
        name: '',
    },
    isOpenModal: false,
};

const INCREASE_STEP = '[STEP] Change Step';
const DECREASE_STEP = '[STEP] Decrease Step';
const CHANGE_DATA = '[DATA] Change Data';
const TOGGLE_MODAL = "[MODAL] Toggle Modal";

export const increaseStep = () => ({
    type: INCREASE_STEP,
});

export const decreaseStep = () => ({
    type: DECREASE_STEP,
});

export const changeData = (data) => ({
    type: CHANGE_DATA,
    payload: data,
});

export const toggleModal = () => ({
    type: TOGGLE_MODAL,
});

export const LoginFormReducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREASE_STEP:
            return {
                ...state,
                step: state.step + 1,
            };
        case DECREASE_STEP:
            return {
                ...state,
                step: state.step - 1,
            };
        case CHANGE_DATA:
            return {
                ...state,
                ...action.payload,
            }
        case TOGGLE_MODAL:
            return {
                ...state,
                isOpenModal: !state.isOpenModal,
            }
        default:
            return state;
    };
};