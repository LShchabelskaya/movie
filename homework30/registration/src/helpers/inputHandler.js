import { changeData } from '../reducer/LoginFormReducer';

export const inputHandler = (event, dispatch) => {
    dispatch(changeData({ [event.target.id]: event.target.value }));
};