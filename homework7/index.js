//Задача 1
//Напишіть функцію parseDate(), яка показує поточну дату і час чітко в заданому форматі.
//Today is: Thursday.
//Current time is: 10 PM : 03 : 07

function parseDate() {
  const currentDate = new Date();
  const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const currentDay = weekDays[currentDate.getDay()]; 
  const currentHours = formatHours(currentDate.getHours());
  const currentMinutes = addZero(currentDate.getMinutes());
  const currentSeconds = addZero(currentDate.getSeconds());
  return `Today is: ${currentDay}.\nCurrent time is: ${currentHours} : ${currentMinutes} : ${currentSeconds}`;
};

function addZero(value) {
    return (value < 10) ? '0' + value : value;
};

function formatHours(value) {
  if(value === 0) {
    return (value + 12) + ' AM';
  } else if(value > 0 && value < 12) {
    return value + ' AM';
  } else if(value === 12) {
    return value + ' PM';
  } else {
    return (value - 12) + ' PM';
  }
};

console.log(parseDate());

//Задача 2
//Напишіть функцію getRandomInteger(min, max), яка повертає ціле число в заданому діапазоні чисел, які передані в аргументах функції.
//Напишіть програму, яка питає у користувача ціле число і порівнює його зі створеним числом за допомогою функції getRandomInteger.
//Якщо користувач ввів невірне число, виведіть в консоль повідомлення про помилку.
//Якщо числа співпадають, вивести в консоль Good work, якщо ні - Not matched.

function getRandomInteger(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const randomIntNumber = getRandomInteger(1, 5);
const userNumberAsStr = prompt('Please enter an integer number:', '5');
const userNumber = +userNumberAsStr;

if(isNaN(userNumber) || !Number.isInteger(userNumber) || isNaN(parseFloat(userNumberAsStr))) {
  console.error('Please use integer numbers only!');
} else if(randomIntNumber === userNumber) {
  console.log('Good work');
} else {
  console.log('Not matched');
}

//Задача 3
//Напишіть функцію getDecimalNumber(arr), яка приймає в якості аргументу масив з чисел 0 або 1, 
//і повертає число в десятичній системі еквівалентне заданому.
//Наприклад, массив [0, 0, 0, 1] розглядається як 0001 і дорівнює 1.
//Приклади для перевірки:
//getDecimalNumber([0, 0, 0, 1]) // 1
//getDecimalNumber([0, 0, 1, 0]) // 2
//getDecimalNumber([1, 1, 1, 1]) // 15
//getDecimalNumber([1, 1, 1, 0, 0, 1]) // 57
//Массив може бути довільної довжини. В масиві не повинно бути ніяких других даних окрім чисел 0 і 1. 
//В цій задачі забороняється використовувати number.toString(), давайте спробуємо написати алгоритм самостійно :)

function getDecimalNumber(arr) {
  let result = 0;
  for(let i = 0; i < arr.length; i++) {
    if(!(arr[i] === 0 || arr[i] === 1)) {
      return 'Please use array with 0 and 1 numbers only!';
    }
    result += arr[i] * 2 ** (arr.length - 1 - i);
  }
  return result;
};

console.log(getDecimalNumber([1, 1, 1, 0, 0, 1]));

//Задача 4
//Напишіть програму, яка питає у користувача число і ділить його на 2 стільки разів, 
//поки воно не буде <= 50. Виведіть в консоль фінальне число і кількість операцій, які знадобились, 
//щоб досягти цього числа.
//Приклад виконання програми для number = 100500;
//Initial number is: 100500;
//Attempts: 11;
//Final number is: 49.072265625;

const userNumToDivideStr = prompt('Please enter a positive number:', '500');
const userNumberToDivide = +userNumToDivideStr;
const minNumberDivided = 50;

if(isNaN(userNumberToDivide) || userNumberToDivide < 0 || isNaN(parseFloat(userNumToDivideStr))) {
  console.error('Please use positive numbers only!');
} else {
  console.log(divideUntil50(userNumberToDivide));
}

function divideUntil50(number) {
  let attemptsQuantity = 0;
  for(let i = 1; number >= minNumberDivided; i++) {
    number = number / 2;
    attemptsQuantity++;
  }
  return `Initial number is: ${userNumberToDivide};\nAttempts: ${attemptsQuantity};\nFinal number is: ${number}`;
};

//Задача 5
//Напишіть функцію getExponent(number, exponent), яка приймає два аргументи число і степінь 
//і возводить це число в задану степінь (як Math.pow(), яле без Math.pow() i оператора **).
//Вирішить задачу двома способами: через цикл і через рекурсію.
//getExponent(2,3) // 8
//getExponent(3,6) // 729
//getExponent(0,0) // 1

//a) через цикл:

function getExponent(number, exponent) {
  let result = 1;
  for(let i = 1; i <= exponent; i++) {
    result *= number;
  }
  return result;
};

//б) через рекурсію:

function getExponent(number, exponent) {
  return exponent > 0 ? number * getExponent(number, exponent - 1) : 1;
};

console.log(getExponent(3, 6));